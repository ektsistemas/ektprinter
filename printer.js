const { Tray, ipcRenderer } = require('electron');
const fs = require('fs');
const OS = require('os');
const escpos = require('escpos');
const Promise = require('bluebird');
const latinize = require('latinize');
const qrcode = Promise.promisify(require('qrcode').toFile);
const Store = require('electron-store');
const receipt = require('./receipt');
const util = require('./util.js');
const dadosNfce = require('./dadosNfce.js');
const store = new Store();

latinize.characters['º'] = 'o';
latinize.characters['ª'] = 'a';

module.exports = async (xml) => {
  const width = parseInt(store.get('caracteres')) || 64;
  const savedVId = parseInt(store.get('vendorId'));
  const savedPId = parseInt(store.get('productId'));
  const posicaoQrCode = store.get('posicaoQrCode') || 'ct';
  const fonte = store.get('fonte') || 'b';

  const nota = await dadosNfce(xml);

  receipt.config.width = width;
  receipt.config.ruler = '-';

  let lines = [
    {
      type: 'text', value: [
        '~' + nota.emissor.cnpj + ' ' + nota.emissor.nome,
        '~' + nota.emissor.rua + ', ' + nota.emissor.numero,
        '~' + nota.emissor.bairro + ', ' + nota.emissor.cidade + ', ' + nota.emissor.uf,
        nota.emissor.nome == 'TESTE DE IMPRESSÃO EM CUPOM' ?
        '~Documento de teste de impressão em cupom.' :
        '~Documento Auxiliar da Nota Fiscal de Consumidor Eletrônica'
      ], align: 'center'
    },
    { type: 'empty' },
    { type: 'table', lines: nota.produtos },
    { type: 'empty' },
    {
      type: 'properties', lines: (() => {
        let l = [
          { name: 'Quantidade total de itens', value: nota.total.itens },
          { name: 'Valor Total R$', value: nota.total.valor },
          { name: 'Desconto R$', value: nota.total.desconto },
          { name: '~Valor a Pagar R$', value: nota.total.nota },
          { name: 'FORMA PAGAMENTO', value: 'VALOR PAGO R$' },
        ];

        nota.pagamento.forEach((pagamento) => {
          l.push({ name: pagamento.forma, value: pagamento.valor });
        });
        
        l.push({ name: 'Troco R$', value: nota.pagamento[0].troco });
        return l;
      })()
    }
  ];

  lines.push({
    type: 'text', value: [
      '~Consulta pela Chave de Acesso em',
      nota.nota.urlConsulta,
      width > 54 ?
        util.mask(nota.nota.chave, '#### #### #### #### #### #### #### #### #### #### ####') :
        nota.nota.chave,
    ], align: 'center'
  });
  lines.push({ type: 'empty' });

  if (nota.consumidor && nota.consumidor.identificador)
    lines.push({ type: 'text', value: '~Consumidor - ' + nota.consumidor.identificador, align: 'center' });
  else
    lines.push({ type: 'text', value: '~Consumidor não identificado', align: 'center' });

  lines.push({
    type: 'text', value: [
      'NFC-e ' + nota.nota.numero,
      'Serie ' + nota.nota.serie,
      nota.nota.data,
      '~'
    ].join('  '), align: 'center'
  });

  lines.push({
    type: 'text', value: '~Protocolo de autorizacao: ' + util.mask(nota.nota.autorizacao.numero, '### ########## ##'), align: 'center'
  });

  lines.push({
    type: 'text', value: '~Data de autorizacao: ' + nota.nota.autorizacao.data, align: 'center'
  });

  lines.push({ type: 'empty' });
  lines.push({ type: 'text', value: '-----QRCODE-----', align: 'center' });
  lines.push({ type: 'empty' });

  if (nota.nota.observacao)
    lines.push({ type: 'text', value: nota.nota.observacao, align: 'left' });

    const output = receipt.create(lines);
    
  const device = await (new escpos.USB(savedVId, savedPId));
  const printer = new escpos.Printer(device, { encoding: "utf8" });
  
  const qrcodeFile = OS.tmpdir() + '/' + nota.nota.numero + '.png';  
  await qrcode(qrcodeFile, nota.nota.qrcode);
  const image = await (new Promise((resolve, reject) => {
    escpos.Image.load(qrcodeFile, (i) => resolve(i));
  }));

  await (new Promise((resolve, reject) => {
    device.open(() => resolve());
  }));

  ipcRenderer.send('displayBalloon', { title: 'EKT Sistemas', content: 'Imprimindo cupom.' });

  await printer.print('\x1c\x2e') // FS .
  await printer.print('\x1b\x74\x08') // ESC t n 8(M58-LL CodePage:Hebrew)

  printer
    .font(fonte)
    .align('lt');

  output.split('\r\n').forEach((l) => {
    if (l.indexOf('-----QRCODE-----') > -1) {
      printer
        .align(posicaoQrCode)
        .image(image)
        .align('lt')
        .style('NORMAL');
    } else {
      if (l.indexOf('~') > -1) {
        printer.style('B');
        l = l.replace(/~/g, '')
      }

      printer.text(latinize(l))
      printer.style('NORMAL');
    }
  });

  printer
    .align('rt')
    .font('b')
    .text('http://ektsistemas.com.br')
    .align('lt')
    .cut();

  await (new Promise((resolve, reject) => {
    printer.close(() => resolve());
  }));

  fs.unlinkSync(qrcodeFile);

  return output;
};