'use strict';

const Promise = require('bluebird');
const xml2js = Promise.promisify(require('xml2js').parseString);
const moment = require('moment');
const util = require('./util.js');

module.exports = async function(xml) {
  let nfce = await xml2js(xml);

  let retorno = {
    nota: {},
    emissor: {},
    produtos: [],
    total: {},
    pagamento: [],
    consumidor: {},
  };

  let totalItens = 0;
  let infNFe = nfce.nfeProc.NFe[0].infNFe[0];
  let infProt = nfce.nfeProc.protNFe[0].infProt[0];
  let qrCode = nfce.nfeProc.NFe[0].infNFeSupl[0].qrCode[0];

  retorno.nota = {
    chave: infNFe.$.Id.replace(/[^0-9]/g, ''),
    numero: util.str_pad(infNFe.ide[0].nNF[0], 9, '0'),
    serie: util.str_pad(infNFe.ide[0].serie[0], 3, '0'),
    data: moment(infNFe.ide[0].dhEmi[0]).format('DD/MM/YYYY HH:mm:ss'),
    ambiente: parseInt(infNFe.ide[0].tpAmb[0]),
    autorizacao: {
      data: moment(infProt.dhRecbto[0]).format('DD/MM/YYYY HH:mm:ss'),
      numero: infProt.nProt[0]
    },
    qrcode: qrCode,
    urlConsulta: util.urlConsulta(parseInt(infNFe.ide[0].tpAmb[0]), infNFe.emit[0].enderEmit[0].UF[0]),
  };

  if (infNFe.infAdic && infNFe.infAdic[0].infCpl)
    retorno.nota.observacao = infNFe.infAdic[0].infCpl[0];

  retorno.emissor = {
    nome: infNFe.emit[0].xNome[0],
    cnpj: util.mask_cnpj(infNFe.emit[0].CNPJ[0]),
    rua: infNFe.emit[0].enderEmit[0].xLgr[0],
    numero: infNFe.emit[0].enderEmit[0].nro[0],
    bairro: infNFe.emit[0].enderEmit[0].xBairro[0],
    cidade: infNFe.emit[0].enderEmit[0].xMun[0],
    uf: infNFe.emit[0].enderEmit[0].UF[0]
  }

  infNFe.det.forEach((produto) => {
    let desconto = 0;
    if (produto.prod[0].vDesc)
      desconto = produto.prod[0].vDesc[0];

    retorno.produtos.push({
      codigo: produto.prod[0].cProd[0],
      descricao: produto.prod[0].xProd[0],
      quantidade: util.number_format(produto.prod[0].qCom[0], 2, ',', '.'),
      valor: util.number_format(produto.prod[0].vUnCom[0], 2, ',', '.'),
      valorTotal: util.number_format(produto.prod[0].vProd[0], 2, ',', '.'),
      desconto: util.number_format(desconto, 2, ',', '.'),
      unidade: produto.prod[0].uCom[0]
    });

    totalItens += parseFloat(produto.prod[0].qCom[0]);
  });

  retorno.total = {
    itens: util.number_format(totalItens, 2, ',', '.'),
    valor: util.number_format(infNFe.total[0].ICMSTot[0].vProd[0], 2, ',', '.'),
    desconto: util.number_format(infNFe.total[0].ICMSTot[0].vDesc[0], 2, ',', '.'),
    nota: util.number_format(infNFe.total[0].ICMSTot[0].vNF[0], 2, ',', '.'),
  }

  let troco = 0;
  infNFe.pag.forEach((pag) => {
    troco += parseFloat(pag.vPag[0]);
    retorno.pagamento.push({
      forma: util.formaPagamento(pag.tPag[0]),
      valor: util.number_format(pag.vPag[0], 2, ',', '.')
    });
  });

  troco -= parseFloat(infNFe.total[0].ICMSTot[0].vNF[0]);
  retorno.pagamento[0].troco = util.number_format(troco, 2, ',', '.');

  if (infNFe.dest) {
    retorno.consumidor = { }
    if (infNFe.dest[0].CNPJ)
      retorno.consumidor.identificador = 'CNPJ ' + util.mask_cnpj(infNFe.dest[0].CNPJ[0]);
    if (infNFe.dest[0].CPF)
      retorno.consumidor.identificador = 'CPF ' + util.mask_cpf(infNFe.dest[0].CPF[0]);

    if (infNFe.dest[0].xNome)
      retorno.consumidor.nome = infNFe.dest[0].xNome[0];
  }

  return retorno;
};