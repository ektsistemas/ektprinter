'use strict';

const EOL = require('os').EOL;
const utils = require('./utils');
const Store = require('electron-store');
const store = new Store();

module.exports = {
	empty(chunk) {
		return utils.pad('', ' ', this.config.width, utils.PAD_RIGHT);
	},

	ruler(chunk) {
		return utils.pad('', this.config.ruler, this.config.width, utils.PAD_RIGHT);
	},

	text(chunk) {
		if (Array.isArray(chunk.value)) {
			// Expand array to multiple text calls with same formatting.
			return chunk.value.map((value) => {
				return this.formatters.text({ type: chunk.type, value: value, align: chunk.align, padding: chunk.padding });
			}).join(EOL);
		}

		let chars = this.config.width - (chunk.padding ? (chunk.align === 'center' ? chunk.padding * 2 : chunk.padding) : 0);

		let isBold = chunk.value.indexOf('~') > -1;
		let words = chunk.value.split(/\s/g);
		let lines = [];
		let line = '';

		words.reverse();

		while (words.length > 0) {
			let word = words.pop().trim();
			word = word.replace(/~/g, '');
			if (line.length + word.replace(/~/g, '').length > chars) {
				lines.push(isBold ? '~' + line : line);
				line = '';
			}

			if (isBold && line == '' && word.indexOf('~') === -1)
				line += '~';

			line += word + ' ';
		}

		lines.push(line);

		let alignTypes = {
			left: utils.PAD_RIGHT,
			right: utils.PAD_LEFT,
			center: utils.PAD_BOTH
		};

		if (lines) {
			return lines.map((line) => {
				line = line.replace(/\s+$|^\s+/, '');

				if (chunk.hasOwnProperty('align')) {
					if (alignTypes.hasOwnProperty(chunk.align)) {
						return utils.pad(line, ' ', this.config.width, alignTypes[chunk.align]);
					}
				}

				return utils.pad(line, ' ', this.config.width, utils.PAD_RIGHT);
			}).join(EOL);
		}

		return '';
	},

	properties(chunk) {
		return chunk.lines.map((line) => {
			let nameIsBold = line.name.indexOf('~') > -1;
			let valueIsBold = line.value.indexOf('~') > -1;
			line.name = line.name.replace(/~/g, '');
			line.value = line.value.replace(/~/g, '');
			const width = this.config.width - (line.value).length;
			return (nameIsBold ? '~' : '') + utils.pad(line.name + ':', ' ', width) + (valueIsBold ? '~' : '') + line.value;
		}).join(EOL);
	},

	table(chunk) {

		let tamanhoCodigo = parseInt(store.get('caracteresCod')) || 13;
		let tamanhoQt = parseInt(store.get('caracteresQt')) || 5;
		let tamanhoUn = parseInt(store.get('caracteresUn')) || 2;
		let tamanhoValor = parseInt(store.get('caracteresVlUnit')) || 8;
		let tamanhoTotal = parseInt(store.get('caracteresVlTot')) || 8;
		let descricaoBreak = store.get('descricaoBreak');
		let showDesconto = store.get('showDesconto');
		descricaoBreak = descricaoBreak == undefined ? true : descricaoBreak == 'true';
		showDesconto = showDesconto == undefined ? true : showDesconto == 'true';

		// pega o tamanho da maior string de cada coluna.
		const maiorCodigo = ['Cod.'.length].concat(chunk.lines.map((l) => l.codigo.trim().length)).reduce((a, b) => Math.max(a, b));
		const maiorQt = ['Qt.'.length].concat(chunk.lines.map((l) => l.quantidade.trim().length)).reduce((a, b) => Math.max(a, b));
		const maiorUn = ['UN'.length].concat(chunk.lines.map((l) => l.unidade.trim().length)).reduce((a, b) => Math.max(a, b));
		const maiorValor = ['Vl Unit.'.length].concat(chunk.lines.map((l) => l.valor.trim().length)).reduce((a, b) => Math.max(a, b));
		const maiorTotal = ['Vl Total'.length].concat(chunk.lines.map((l) => l.valorTotal.trim().length)).reduce((a, b) => Math.max(a, b));

		tamanhoCodigo = maiorCodigo < tamanhoCodigo ? maiorCodigo : tamanhoCodigo;
		tamanhoQt = maiorQt < tamanhoQt ? maiorQt : tamanhoQt;
		tamanhoUn = maiorUn < tamanhoUn ? maiorUn : tamanhoUn;
		tamanhoValor = maiorValor < tamanhoValor ? maiorValor : tamanhoValor;
		tamanhoTotal = maiorTotal < tamanhoTotal ? maiorTotal : tamanhoTotal;

		let lines = [];
		let tamanhoDescricao = this.config.width - (tamanhoCodigo + tamanhoQt + tamanhoUn + tamanhoValor + tamanhoTotal + 5);

		lines.push([
			'~' + utils.pad('Cod.', ' ', tamanhoCodigo, utils.PAD_LEFT) + ' ',
			utils.pad('Desc.', ' ', tamanhoDescricao, utils.PAD_RIGHT),
			utils.pad('Qt.', ' ', tamanhoQt + 1, utils.PAD_LEFT),
			utils.pad('UN', ' ', tamanhoUn + 1, utils.PAD_LEFT),
			utils.pad('Vl Unit.', ' ', tamanhoValor + 1, utils.PAD_LEFT),
			utils.pad('Vl Total', ' ', tamanhoTotal + 1, utils.PAD_LEFT)
		].join(''));

		for (let line of chunk.lines) {
			line.codigo = line.codigo.trim();
			line.descricao = line.descricao.trim();
			line.unidade = line.unidade.trim();
			line.quantidade = line.quantidade.trim();
			line.valor = line.valor.trim();
			line.valorTotal = line.valorTotal.trim();

			const numeroLinhas = [
				Math.ceil(line.codigo.length / tamanhoCodigo),
				descricaoBreak ? Math.ceil(line.descricao.length / tamanhoDescricao) : 1,
				Math.ceil(line.unidade.length / tamanhoUn),
				Math.ceil(line.quantidade.length / tamanhoQt),
				Math.ceil(line.valor.length / tamanhoValor),
				Math.ceil(line.valorTotal.length / tamanhoTotal),
			].reduce(function (a, b) {
				return Math.max(a, b);
			});

			for(var i = 1; i <= numeroLinhas; i++) {

				let codigo     = line.codigo    .substr((i * tamanhoCodigo)    - tamanhoCodigo,    tamanhoCodigo);
				let descricao  = line.descricao .substr((i * tamanhoDescricao) - tamanhoDescricao, tamanhoDescricao);
				let unidade    = line.unidade   .substr((i * tamanhoUn)        - tamanhoUn,        tamanhoUn);
				let quantidade = line.quantidade.substr((i * tamanhoQt)        - tamanhoQt,        tamanhoQt);
				let valor      = line.valor     .substr((i * tamanhoValor)     - tamanhoValor,     tamanhoValor);
				let valorTotal = line.valorTotal.substr((i * tamanhoTotal)     - tamanhoTotal,     tamanhoTotal);

				if (descricaoBreak == false && i > 1)
					descricao = '';

				lines.push([
					utils.pad(codigo, ' ', tamanhoCodigo, utils.PAD_LEFT) + ' ',
					utils.pad(descricao, ' ', tamanhoDescricao, utils.PAD_RIGHT),
					utils.pad(quantidade, ' ', tamanhoQt + 1, utils.PAD_LEFT),
					utils.pad(unidade, ' ', tamanhoUn + 1 , utils.PAD_LEFT),
					utils.pad(valor, ' ', tamanhoValor + 1, utils.PAD_LEFT),
					utils.pad(valorTotal, ' ', tamanhoTotal + 1, utils.PAD_LEFT)
				].join(''));
			}

			if (line.desconto.toFloat() > 0 && showDesconto) {
				let desconto = '(Desconto: ' + line.desconto + ')';
				lines.push([
					utils.pad('', ' ', tamanhoCodigo + 1, utils.PAD_RIGHT),
					utils.pad(desconto, ' ', this.config.width - desconto.length - tamanhoCodigo - 1, utils.PAD_RIGHT),
				].join(''));
			}
		}

		return lines.join(EOL);
	}
};

String.prototype.toFloat = function () {
	let val = this;
	val = val.replace(/\./g, '').replace(/\,/g, '.');
	return parseFloat(val);
};