const { ipcRenderer } = require('electron');
const { dialog } = require('electron').remote
const request = require('request');
const fs = require('fs');
const usbDetect = require('usb-detection');
const Store = require('electron-store');
const PQueue = require('p-queue');
const printer = require('./printer.js');
const util = require('./util.js');
const store = new Store();
const queue = new PQueue({ concurrency: 1 });

const deviceList = document.getElementById('deviceList');
const caracteres = document.getElementById('caracteres');
const caracteresCod = document.getElementById('caracteresCod');
const caracteresQt = document.getElementById('caracteresQt');
const caracteresUn = document.getElementById('caracteresUn');
const caracteresVlUnit = document.getElementById('caracteresVlUnit');
const caracteresVlTot = document.getElementById('caracteresVlTot');
    
let savedFont = store.get('fonte') || 'b';
let savedPosicaoQrCode = store.get('posicaoQrCode') || 'ct';
let savedCaracteres = store.get('caracteres') || 64;
let savedCaracteresCod = store.get('caracteresCod') || 13;
let savedCaracteresQt = store.get('caracteresQt') || 5;
let savedCaracteresUn = store.get('caracteresUn') || 2;
let savedCaracteresVlUnit = store.get('caracteresVlUnit') || 8;
let savedCaracteresVlTot = store.get('caracteresVlTot') || 8;
let savedDescricaoBreak = store.get('descricaoBreak');
let savedShowDesconto = store.get('showDesconto');
savedDescricaoBreak = savedDescricaoBreak == undefined ? 'true' : savedDescricaoBreak;
savedShowDesconto = savedShowDesconto == undefined ? 'true' : savedShowDesconto;

document.querySelectorAll('[name=fonte]').forEach((radio) =>{
  radio.checked = radio.value == savedFont;
});

document.querySelectorAll('[name=posicaoQrCode]').forEach((radio) =>{
  radio.checked = radio.value == savedPosicaoQrCode;
});

document.querySelectorAll('[name=descricaoBreak]').forEach((radio) =>{
  radio.checked = radio.value == savedDescricaoBreak;
});

document.querySelectorAll('[name=showDesconto]').forEach((radio) => {
  radio.checked = radio.value == savedDescricaoBreak;
});

caracteres.value = savedCaracteres;
caracteresCod.value = savedCaracteresCod;
caracteresQt.value = savedCaracteresQt;
caracteresUn.value = savedCaracteresUn;
caracteresVlUnit.value = savedCaracteresVlUnit;
caracteresVlTot.value = savedCaracteresVlTot;

document.getElementById('save').onclick = () => {
  let device = deviceList.options[deviceList.selectedIndex].value;
  let fonte = document.querySelector('[name=fonte]:checked').value;
  let posicaoQrCode = document.querySelector('[name=posicaoQrCode]:checked').value;
  let descricaoBreak = document.querySelector('[name=descricaoBreak]:checked').value;
  let showDesconto = document.querySelector('[name=showDesconto]:checked').value;

  let chars = parseInt(caracteres.value);
  let charsItens = parseInt(caracteresCod.value) + 1 +
    parseInt(caracteresQt.value) + 1 +
    parseInt(caracteresUn.value) + 1 +
    parseInt(caracteresVlUnit.value) + 1 +
    parseInt(caracteresVlTot.value) + 1;

  if (charsItens > chars)
    return dialog.showMessageBox({ type: 'error', title: 'Erro!', message: 'A soma dos caracteres ultrapassa o número de caracteres por linha' })

  store.set('vendorId', device.split('-')[0]);
  store.set('productId', device.split('-')[1]);
  store.set('fonte', fonte);
  store.set('posicaoQrCode', posicaoQrCode);
  store.set('caracteres', caracteres.value);
  store.set('caracteresCod', caracteresCod.value);
  store.set('caracteresQt', caracteresQt.value);
  store.set('caracteresUn', caracteresUn.value);
  store.set('caracteresVlUnit', caracteresVlUnit.value);
  store.set('caracteresVlTot', caracteresVlTot.value);
  store.set('descricaoBreak', descricaoBreak);
  store.set('showDesconto', showDesconto);
};

document.getElementById('cancel').onclick = () => { location.reload(); };
document.getElementById('test').onclick = () => {
  printIt(util.xmlTeste);
};

function getDevices () {
  deviceList.querySelectorAll('option').forEach(option => option.remove());

  usbDetect.find(function (err, devices) {
    let savedVId = store.get('vendorId');
    let savedPId = store.get('productId');
    
    deviceList.options.add(
      new Option('Selecione', '', savedVId && savedPId, savedVId && savedPId)
    );

    devices.forEach((device) => {
      let vId = device.vendorId,
        pId = device.productId,
        name = device.deviceName;

      deviceList.options.add(
        new Option(
          device.deviceName,
          `${vId}-${pId}`,
          vId == savedVId && pId == savedPId,
          vId == savedVId && pId == savedPId
        )
      );
    });
  });
}

getDevices();
usbDetect.startMonitoring();
usbDetect.on('change', getDevices);
document.getElementById('refresDeviceList').onclick = getDevices;

ipcRenderer.on('downloadAndPrint', function (event, url) {
  request(url, function name(err, response, body) {
    queue.add(() => printIt(body));
  });
});

async function printIt (xml) {
  try {
    let text = await printer(xml);
    ipcRenderer.send('printDone', 'done');
  } catch (err) {
    console.log(err);
    let message = err.message;
    if (message.includes('Can not find printer'))
      message = 'Impressora não encontrada.\nVerifique se ela está ligada ou se os drivers foram instalados corretamente.';

    return dialog.showMessageBox({ type: 'error', title: 'Erro!', message: message });
  }
}