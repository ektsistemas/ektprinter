function mask(val, mask) {
  maskared = '';
  k = 0;
  for (i = 0; i <= mask.length - 1; i++) {
    if (mask[i] == '#') {
      if (val[k])
        maskared += val[k++];
    }
    else {
      if (mask[i])
        maskared += mask[i];
    }
  }
  return maskared;
}

function urlConsulta(ambiente, uf) {

  let urls = {
    '1': {
      'AC': 'www.sefaznet.ac.gov.br/nfce/consulta',
      'AL': 'http://nfce.sefaz.al.gov.br/consultaNFCe.htm',
      'AP': 'https://www.sefaz.ap.gov.br/sate/seg/SEGf_AcessarFuncao.jsp?cdFuncao=FIS_1261',
      'AM': 'sistemas.sefaz.am.gov.br/nfceweb/formConsulta.do',
      'BA': 'nfe.sefaz.ba.gov.br/servicos/nfce/default.aspx',
      'CE': '',
      'DF': 'http://dec.fazenda.df.gov.br/NFCE/',
      'ES': 'http://app.sefaz.es.gov.br/ConsultaNFCe',
      'GO': '',
      'MA': 'http://www.nfce.sefaz.ma.gov.br/portal/consultaNFe.do?method=preFilterCupom&',
      'MT': 'http://www.sefaz.mt.gov.br/nfce/consultanfce',
      'MS': 'http://www.dfe.ms.gov.br/nfce',
      'MG': '',
      'PA': 'https://appnfc.sefa.pa.gov.br/portal/view/consultas/nfce/consultanfce.seam',
      'PB': 'www.receita.pb.gov.br/nfce',
      'PR': 'http://www.fazenda.pr.gov.br',
      'PE': '',
      'PI': 'http://webas.sefaz.pi.gov.br/nfceweb/consultarNFCe.jsf',
      'RJ': 'www.nfce.fazenda.rj.gov.br/consulta',
      'RN': 'http://nfce.set.rn.gov.br/consultarNFCe.aspx',
      'RS': 'https://www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx',
      'RO': 'http://www.nfce.sefin.ro.gov.br',
      'RR': 'https://www.sefaz.rr.gov.br/nfce/servlet/wp_consulta_nfce',
      'SC': '',
      'SP': 'https://www.nfce.fazenda.sp.gov.br/NFCeConsultaPublica/Paginas/ConsultaPublica.aspx',
      'SE': 'http://www.nfce.se.gov.br/portal/portalNoticias.jsp',
      'TO': '',
      '99': 'http://teste.com.br/consulta'
    },
    '2': {
      'AC': 'http://hml.sefaznet.ac.gov.br/nfce/consulta',
      'AL': 'http://nfce.sefaz.al.gov.br/consultaNFCe.htm',
      'AP': 'https://www.sefaz.ap.gov.br/sate1/seg/SEGf_AcessarFuncao.jsp?cdFuncao=FIS_1261',
      'AM': 'homnfce.sefaz.am.gov.br/nfceweb/formConsulta.do',
      'BA': 'http://hnfe.sefaz.ba.gov.br/servicos/nfce/default.aspx',
      'CE': 'http://nfceh.sefaz.ce.gov.br/pages/consultaNota.jsf',
      'DF': 'http://dec.fazenda.df.gov.br/NFCE/',
      'ES': 'http://homologacao.sefaz.es.gov.br/ConsultaNFCe',
      'GO': '',
      'MA': 'http://www.hom.nfce.sefaz.ma.gov.br/portal/consultarNFCe.jsp',
      'MT': 'http://homologacao.sefaz.mt.gov.br/nfce/consultanfce',
      'MS': 'http://www.dfe.ms.gov.br/nfce',
      'MG': '',
      'PA': 'https://appnfc.sefa.pa.gov.br/portal-homologacao/view/consultas/nfce/consultanfce.seam',
      'PB': '',
      'PR': 'http://www.fazenda.pr.gov.br',
      'PE': '',
      'PI': 'http://webas.sefaz.pi.gov.br/nfceweb-homologacao/consultarNFCe.jsf',
      'RJ': 'www.nfce.fazenda.rj.gov.br/consulta',
      'RN': 'http://nfce.set.rn.gov.br/consultarNFCe.aspx',
      'RS': 'https://www.sefaz.rs.gov.br/NFCE/NFCE-COM.aspx',
      'RO': 'http://www.nfce.sefin.ro.gov.br',
      'RR': 'http://200.174.88.103:8080/nfce/servlet/wp_consulta_nfce',
      'SC': '',
      'SP': 'https://www.homologacao.nfce.fazenda.sp.gov.br/NFCeConsultaPublica/Paginas/ConsultaPublica.aspx',
      'SE': 'http://www.hom.nfe.se.gov.br/portal/portalNoticias.jsp',
      'TO': '',
      '99': 'http://teste.com.br/consulta'
    },
  };

  return urls[ambiente][uf];
}

function formaPagamento(codigo) {
  return ({
    1: 'Dinheiro',
    2: 'Cheque',
    3: 'Cartão de Crédito',
    4: 'Cartão de Débito',
    5: 'Crédito Loja',
    10: 'Vale Alimentação',
    11: 'Vale Refeição',
    12: 'Vale Presente',
    13: 'Vale Combustível',
  })[parseInt(codigo)] || 'Outros'
}

function mask_cpf(v) {
  v = v + "";
  v = v.replace(/\D/g, "")
  return mask(v, '###.###.###-##');
}

function mask_cnpj(v) {
  v = v + "";
  v = v.replace(/\D/g, "")
  return mask(v, '##.###.###/####-##');
}

function number_format(number, decimals, dec_point, thousands_sep) {
  //  discuss at: http://phpjs.org/functions/number_format/
  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 2 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}

function str_pad(input, pad_length, pad_string, pad_type) {
  //  discuss at: http://phpjs.org/functions/str_pad/
  var half = '',
    pad_to_go;

  var str_pad_repeater = function (s, len) {
    var collect = '',
      i;

    while (collect.length < len) {
      collect += s;
    }
    collect = collect.substr(0, len);

    return collect;
  };

  input += '';
  pad_string = pad_string !== undefined ? pad_string : ' ';

  if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
    pad_type = 'STR_PAD_LEFT';
  }
  if ((pad_to_go = pad_length - input.length) > 0) {
    if (pad_type === 'STR_PAD_LEFT') {
      input = str_pad_repeater(pad_string, pad_to_go) + input;
    } else if (pad_type === 'STR_PAD_RIGHT') {
      input = input + str_pad_repeater(pad_string, pad_to_go);
    } else if (pad_type === 'STR_PAD_BOTH') {
      half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
      input = half + input + half;
      input = input.substr(0, pad_length);
    }
  }

  return input;
}

module.exports = {
  mask: mask,
  urlConsulta: urlConsulta,
  formaPagamento: formaPagamento,
  mask_cpf: mask_cpf,
  mask_cnpj: mask_cnpj,
  number_format: number_format,
  str_pad: str_pad,
  xmlTeste: '<?xml version="1.0" encoding="UTF-8"?><nfeProc xmlns="http://www.portalfiscal.inf.br/nfe" versao="3.10"><NFe xmlns="http://www.portalfiscal.inf.br/nfe"><infNFe Id="NFe99999999999999999999999999999999999999999999" versao="3.10"><ide><cUF>99</cUF><cNF>99999999</cNF><natOp>TESTE DE IMPRESSÃO EM CUPOM</natOp><indPag>0</indPag><mod>65</mod><serie>999</serie><nNF>999999999</nNF><dhEmi>2000-01-01T00:00:00-03:00</dhEmi><tpNF>1</tpNF><idDest>1</idDest><cMunFG>9999999</cMunFG><tpImp>4</tpImp><tpEmis>1</tpEmis><cDV>2</cDV><tpAmb>2</tpAmb><finNFe>1</finNFe><indFinal>1</indFinal><indPres>1</indPres><procEmi>0</procEmi><verProc>0.3.0.0</verProc></ide><emit><CNPJ>99999999999999</CNPJ><xNome>TESTE DE IMPRESSÃO EM CUPOM</xNome><enderEmit><xLgr>RUA</xLgr><nro>999</nro><xCpl>.</xCpl><xBairro>BAIRRO</xBairro><cMun>9999999</cMun><xMun>CIDADE</xMun><UF>99</UF><CEP>99999999</CEP><cPais>1058</cPais><xPais>Brasil</xPais><fone>9999999999</fone></enderEmit><IE>9999999999</IE><CRT>3</CRT></emit><dest><CPF>99999999999</CPF><indIEDest>9</indIEDest></dest><det nItem="1"><prod><cProd>0123456789123</cProd><cEAN/><xProd>DESCRIÇÃO DO PRODUTO Nº 1.</xProd><NCM>99999999</NCM><CFOP>9999</CFOP><uCom>PC</uCom><qCom>1.000</qCom><vUnCom>200</vUnCom><vProd>200</vProd><cEANTrib/><uTrib>PC</uTrib><qTrib>1.000</qTrib><vUnTrib>200</vUnTrib><vDesc>0.0000</vDesc><indTot>1</indTot></prod><imposto><ICMS><ICMS60><orig>0</orig><CST>60</CST></ICMS60></ICMS></imposto></det><det nItem="2"><prod><cProd>0123456789124</cProd><cEAN/><xProd>DESCRIÇÃO DO PRODUTO N. 2.</xProd><NCM>99999999</NCM><CFOP>9999</CFOP><uCom>PC</uCom><qCom>5.000</qCom><vUnCom>260</vUnCom><vProd>1300</vProd><cEANTrib/><uTrib>PC</uTrib><qTrib>5.000</qTrib><vUnTrib>260</vUnTrib><vDesc>500</vDesc><indTot>1</indTot></prod><imposto><ICMS><ICMS60><orig>0</orig><CST>60</CST></ICMS60></ICMS></imposto></det><total><ICMSTot><vBC>0.00</vBC><vICMS>0.00</vICMS><vICMSDeson>0.00</vICMSDeson><vBCST>0.00</vBCST><vST>0.00</vST><vProd>1000</vProd><vFrete>0.00</vFrete><vSeg>0.00</vSeg><vDesc>500</vDesc><vII>0.00</vII><vIPI>0.00</vIPI><vPIS>0.00</vPIS><vCOFINS>0.00</vCOFINS><vOutro>0.00</vOutro><vNF>500</vNF></ICMSTot></total><transp><modFrete>9</modFrete></transp><pag><tPag>01</tPag><vPag>200</vPag></pag><pag><tPag>02</tPag><vPag>350</vPag></pag><infAdic><infCpl>TESTE DE IMPRESSÃO</infCpl></infAdic></infNFe><infNFeSupl><qrCode><![CDATA[http://ektsistemas.com.br/?chNFe=99999999999999999999999999999999999999999999&nVersao=100&tpAmb=1&cDest=43051162949&dhEmi=323031382d30342d32305431363a33303a30302d30333a3030&vNF=0500.00&vICMS=0.00&digVal=99999999999999999999999999999999999999999999999999999999&cIdToken=000001&cHashQRCode=9999999999999999999999999999999999999999]]></qrCode></infNFeSupl><Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/><Reference URI="#NFe99999999999999999999999999999999999999999999"><Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/><DigestValue>DigestValue</DigestValue></Reference></SignedInfo><SignatureValue>Signature</SignatureValue><KeyInfo><X509Data><X509Certificate>CERT</X509Certificate></X509Data></KeyInfo></Signature></NFe><protNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="3.10"><infProt><tpAmb>1</tpAmb><verAplic>PR-v3_4_6</verAplic><chNFe>99999999999999999999999999999999999999999999</chNFe><dhRecbto>2000-01-01T00:00:00-03:00</dhRecbto><nProt>999999999999999</nProt><digVal>432432432432432</digVal><cStat>100</cStat><xMotivo>Autorizado o uso da NF-e</xMotivo></infProt></protNFe></nfeProc>'
};