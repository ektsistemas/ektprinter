const { app, BrowserWindow, webContents, ipcMain, Tray, Menu } = require('electron');
const path = require('path');
const url = require('url');
const icon64Path = path.join(__dirname, 'assets/image/icon64.png');
const icon16Path = path.join(__dirname, 'assets/image/icon64.png');

let mainWindow;
let deeplinkingUrl;
let tray;
let printJobSent;

const shouldQuit = app.makeSingleInstance((argv, workingDirectory) => {
  if (process.platform == 'win32')
  deeplinkingUrl = argv.slice(argv[1] == '.' ? 2 : 1);
  
  downloadAndPrint(deeplinkingUrl.join(''));
  logEverywhere("app.makeSingleInstance# " + deeplinkingUrl);
});

if (shouldQuit)
  app.quit();

function createWindow() {
  if (process.platform == 'win32')
    deeplinkingUrl = process.argv.slice(process.argv[1] == '.' ? 2 : 1);

  mainWindow = new BrowserWindow({
    width: 478,
    minWidth : 478,
    maxWidth : 478,
    minHeight: 600,
    icon: icon16Path
  });

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  mainWindow.openDevTools();
  mainWindow.on('closed', function () {
    mainWindow = null;
  });

  mainWindow.on('minimize', function (event) {
    event.preventDefault();
    mainWindow.hide();
  });

  mainWindow.on('close', function (event) {
    if (!app.isQuiting) {
      event.preventDefault();
      mainWindow.hide();
    }

    return false;
  });

  tray = new Tray(icon16Path);
  const contextMenu = Menu.buildFromTemplate([
    { label: 'Configurações', click:  function(){
      mainWindow.show();
    } },
    { label: 'Fechar', click:  function(){
      app.isQuiting = true;
      app.quit();
    } }
  ]);
  tray.setContextMenu(contextMenu);

  tray.on('double-click', () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show();
  });

  mainWindow.webContents.on('did-finish-load', () => downloadAndPrint(deeplinkingUrl.join('')));
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// Log both at dev console and at running node console instance
function logEverywhere(s) {
  if (mainWindow && mainWindow.webContents) {
    mainWindow.webContents.executeJavaScript(`console.log("${s}")`)
  }
}

function downloadAndPrint(printUrl) {
  if (printUrl.indexOf('ektprinter://') == -1)
    return false;

  printUrl = printUrl.replace('ektprinter://', '');
  if (printUrl)
    mainWindow.webContents.send('downloadAndPrint', printUrl);

  return true;
}

ipcMain.on('displayBalloon', (event, data) => {
  tray.displayBalloon({ title: data.title, content: data.content, icon: icon64Path });
});

ipcMain.on('printDone', (event, data) => {
});